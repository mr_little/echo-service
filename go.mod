module echo_server

go 1.13

require (
	github.com/ianschenck/envflag v0.0.0-20140720210342-9111d830d133
	github.com/stretchr/testify v1.7.0
)
