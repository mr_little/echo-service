package main

import (
	"fmt"
	"log"

	"github.com/ianschenck/envflag"

	"echo_server/internal"
)

type config struct {
	BindAddr string
	BindPort int
}

func setupConfigFromEnv() (*config, error) {
	var cfg config

	envflag.StringVar(&cfg.BindAddr, "BIND_ADDR", "", "TCP listen address")
	envflag.IntVar(&cfg.BindPort, "BIND_PORT", 7, "TPC listen port")

	envflag.Parse()

	if cfg.BindPort == 0 {
		return nil, fmt.Errorf("no port configured")
	}

	return &cfg, nil
}

func startTCP(cfg *config) *internal.ServerTCP {
	h := internal.NewServerTCP()

	go func() {
		err := h.ListenAndServe(fmt.Sprintf("%s:%d", cfg.BindAddr, cfg.BindPort))
		if err != nil {
			log.Fatalf("%v", err)
			return
		}
	}()

	return h
}

func main() {
	cfg, err := setupConfigFromEnv()
	if err != nil {
		log.Fatal(err)
	}
	h := startTCP(cfg)
	h.WaitForSignal()
}
