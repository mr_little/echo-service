
# Creates binary
build-tcp:
	go build -o ./cmd/tcp_server ./cmd/tcp/main.go

# Runs tests
test-unit:
	go test ./... -v -race -count=1 -short

# test formats
test-format:
	[ "`go fmt  ./...`" = "" ]
