### TCP Based Echo Service implementation:

## Build and run
```
docker-compose up --build echo_server_tcp
```

## Simple usage
1. Open terminal window 1 and do build and run
2. Open terminal window 2 and run curl request: `curl -v telnet://127.0.0.1:7` and see that connection to `127.0.0.1:7` were established
3. In terminal window 2 put any text, like `123 test` and press enter. Notify that on terminal window 1 you should see your request. Also check on the server response in window 2.
4. In terminal window 2 put `QUIT` to close the connection to server 

## Improvements
* Add UDP server implementation and treat Sever as common interface 
* Add more tests