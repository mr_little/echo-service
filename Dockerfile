FROM golang:1.15

WORKDIR /opt/echo_server
COPY . .
WORKDIR /opt/echo_server/cmd
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o tcp_server tcp/main.go
