package internal

import (
	"bufio"
	"bytes"
	"github.com/stretchr/testify/assert"
	"io"
	"net"
	"net/http"
	"strings"
	"testing"
	"time"
)

func TestGracefulShutdown(t *testing.T) {
	h := NewServerTCP()

	var err error
	go func() {
		err = h.ListenAndServe(":7")
		assert.ErrorIs(t, err, http.ErrServerClosed)
	}()

	time.Sleep(1 * time.Second)
	h.Done()
}

func TestReadAndWrite(t *testing.T) {
	var b bytes.Buffer

	r := bufio.NewReader(strings.NewReader("ONE\n"))
	w := bufio.NewWriter(&b)

	err := ReadAndWrite(w, r, func(str string) {
		assert.Equal(t, "ONE", str)
	})

	assert.NoError(t, err)

	err = ReadAndWrite(w, r, nil)
	assert.ErrorIs(t, err, io.EOF)
}

func TestTCPServer(t *testing.T) {
	h := NewServerTCP()

	go func() {
		_ = h.ListenAndServe(":765")
	}()

	time.Sleep(1 * time.Second)

	conn, err := net.Dial("tcp", ":765")
	assert.NoError(t, err)

	defer func() { _ = conn.Close() }()

	clientReader := bufio.NewReader(strings.NewReader("Line one\nLine 2\n"))
	serverReader := bufio.NewReader(conn)

	err = ReadAndWrite(conn, clientReader, nil)
	assert.NoError(t, err)

	request, err := serverReader.ReadString('\n')
	assert.NoError(t, err)
	assert.Equal(t, "Line one", strings.TrimSpace(request))

	err = ReadAndWrite(conn, clientReader, nil)
	assert.NoError(t, err)

	request, err = serverReader.ReadString('\n')
	assert.NoError(t, err)
	assert.Equal(t, "Line 2", strings.TrimSpace(request))

	err = ReadAndWrite(conn, clientReader, nil)
	assert.ErrorIs(t, err, io.EOF)

	h.Done()
}
