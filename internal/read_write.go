package internal

import (
	"fmt"
	"io"
	"log"
	"strings"
)

const (
	// StopWord if client want to break the connection he basically needs to send this word
	StopWord = "QUIT"
)

var (
	// QuitError simple wrapper, to have possibility to use with errors.Is
	QuitError = fmt.Errorf(StopWord)
)

type writer interface {
	Write(b []byte) (int, error)
}

type reader interface {
	ReadString(d byte) (string, error)
}

// ReadAndWrite used for both client and tcp side, responsible for read data from reader, write it to writer,
// and call onReceive call back if needed
func ReadAndWrite(w writer, r reader, onReceive func(str string)) error {
	request, err := r.ReadString('\n')
	switch err {
	case nil:
		request = strings.TrimSpace(request)
		if request == StopWord {
			return fmt.Errorf("%w", QuitError)
		}

		// If writer is not nil => means we are going to write message to connection
		// Otherwise we are reading message, so that means we only need to print it our
		if w != nil {
			if _, err = w.Write([]byte(request + "\n")); err != nil {
				return fmt.Errorf("failed to send the client request: %w", err)
			}
		}
		if onReceive != nil {
			onReceive(strings.TrimSpace(request))
			//log.Print(strings.TrimSpace(request))
		}

	case io.EOF:
		return fmt.Errorf("EOF, let's clos the connection: %w", err)
	default:
		return fmt.Errorf("client error: %v\n", err)
	}
	return nil
}

// SendAndReceive client function responsible for send message to tcp and receive it's echo
func SendAndReceive(w writer, clientReader, serverReader reader) error {
	for {
		if err := ReadAndWrite(w, clientReader, nil); err != nil {
			return err
		}

		if err := ReadAndWrite(nil, serverReader, func(str string) {
			log.Println(str)
		}); err != nil {
			return err
		}
	}
}
