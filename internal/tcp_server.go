package internal

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

// NewServerTCP constructor for ServerTCP structure
func NewServerTCP() *ServerTCP {
	return &ServerTCP{
		mu:   sync.Mutex{},
		done: make(chan struct{}),
	}
}

// ServerTCP basic structure for TCP server
type ServerTCP struct {
	done chan struct{}
	mu   sync.Mutex
}

// Done close done channel for tcp and force hit to shut down
func (srv *ServerTCP) Done() {
	srv.mu.Lock()
	defer srv.mu.Unlock()

	select {
	case <-srv.done:
		// Already closed. Don't close again.
	default:
		// Safe to close here. We're the only closer, guarded
		// by s.mu.
		close(srv.done)
	}
}

// ListenAndServe main loop of handling ongoing connections
func (srv *ServerTCP) ListenAndServe(address string) error {
	s, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("failed listen address %s: %w", address, err)
	}

	log.Println("Ready to serve!")

	connsChan := clientConns(s)

	for {
		select {
		case <-srv.done:
			return http.ErrServerClosed
		case client := <-connsChan:
			go handleConn(client)
		}

	}
}

// WaitForSignal helper function to handle graceful stop
func (srv *ServerTCP) WaitForSignal() {
	gracefulStop := make(chan os.Signal, 2)
	signal.Notify(gracefulStop, syscall.SIGTERM, syscall.SIGINT)

	select {
	case <-gracefulStop:
		srv.Done()
	}
}

func clientConns(listener net.Listener) chan net.Conn {
	ch := make(chan net.Conn)
	i := 0
	go func() {
		for {
			client, err := listener.Accept()
			if client == nil {
				fmt.Printf("couldn't accept: %v", err)
				continue
			}
			i++
			fmt.Printf("%d: %v <-> %v\n", i, client.LocalAddr(), client.RemoteAddr())
			ch <- client
		}
	}()
	return ch
}

func handleConn(conn net.Conn) {
	defer func() { _ = conn.Close() }()

	for {
		if err := ReadAndWrite(conn, bufio.NewReader(conn), func(str string) {
			log.Println(str)
		}); err != nil {
			if errors.Is(err, QuitError) {
				log.Println("quit")
				return
			}
			if errors.Is(err, io.EOF) {
				return
			}

			log.Printf("error occurred: %v", err)
			return
		}
	}
}
